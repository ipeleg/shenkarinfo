package il.ac.shenkar.shenkarinfo.markers;

import il.ac.shenkar.shenkarinfo.ListAdapter;
import il.ac.shenkar.shenkarinfo.PathHandler;
import il.ac.shenkar.shenkarinfo.R;
import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.events.AddEventActivity;
import il.ac.shenkar.shenkarinfo.events.EventListArray;
import il.ac.shenkar.shenkarinfo.location.LocationHandler;
import il.ac.shenkar.shenkarinfo.location.NavigationHandler;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class MarkersHandler
{
	private ProgressDialog progressDialog;
	private Activity thisActivity;
	private GoogleMap map;
	private Dialog imageDiaglog;
	private LocationHandler locationHandler;
	private PathHandler pathHandler;
	private ListAdapter listAdapter;
	private NavigationHandler navigationHandler;
	private List<ParseObject> markers;
	
	public MarkersHandler(Activity thisActivity, GoogleMap map, LocationHandler locationHandler, PathHandler pathHandler, NavigationHandler navigationHandler)
	{
		this.thisActivity = thisActivity;
		this.map = map;
		this.locationHandler = locationHandler;
		this.pathHandler = pathHandler;
		this.navigationHandler = navigationHandler;
	}

	/**
	 * Method for getting the markers from Parse and setting them on the map
	 */
	public boolean getMarkersFromCloud()
	{
		progressDialog = ProgressDialog.show(thisActivity, "Getting Markers", "Downloading...", false); // Show the user progress dialog while downloading the markers
		ParseQuery<ParseObject> getMarkersQuery = ParseQuery.getQuery("Markers");
		getMarkersQuery.findInBackground(new FindCallback<ParseObject>()
		{
			@Override
			public void done(List<ParseObject> markersFromParse, ParseException arg1)
			{
				MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
				
				for (int i=0 ; i<markersFromParse.size() ; ++i)
				{
					ParseGeoPoint markerLocation = markersFromParse.get(i).getParseGeoPoint("markerPosition"); // Getting the marker coordinates
					double frequency = markersFromParse.get(i).getDouble("frequency");
					final String markerTitle = markersFromParse.get(i).getString("title"); // Getting the marker title
					String markerId = markersFromParse.get(i).getObjectId(); // Getting the marker ID
					
					Vertex newVertex = new Vertex(markerId, markerTitle, frequency, true); // Creating a vertex for each marker on the map
					
					// Adding a new marker to map according to new vertex details
					Marker newMarker = map.addMarker(new MarkerOptions()
										.position(new LatLng(markerLocation.getLatitude(), markerLocation.getLongitude()))
										.title(markerTitle)
										.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
					
					// Creating new event array for the new marker
					EventListArray eventList = new EventListArray(thisActivity, markerTitle);
					
					// Adding the new marker to the singleton array
					markersArray.addMarker(markerTitle, newVertex, newMarker, eventList);
					
					map.setInfoWindowAdapter(adapter);
					map.setOnInfoWindowClickListener(windowClickListener);
				}
				
				markers = markersFromParse; // Setting the markers from parse in the activity markers field
				new GetMarkersImagesFromParse().execute(); // Calling the async task for downloading the images of the markers
			}
		});
		
		return false;
	}
	
	/**
	 * Method for double check the user.
	 * If there is a current navigation running ask the user if he would like to stop the current one and create a new one
	 * @param markerTitle
	 */
	private void askForNewNavigation(final String markerTitle)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(thisActivity);
		alertDialogBuilder.setTitle("Navigation In Process");
		alertDialogBuilder.setMessage("Stop current navigation?")
							.setCancelable(false)
							.setPositiveButton("Yes", new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									navigationHandler.stopNavigation();
									locationHandler.findCurrentLocation(null, markerTitle);
								}
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									return;
								}
							})
							.create()
							.show();
	}
	
	/**
	 * Window click listener for the info window of each marker on the map
	 */
	private OnInfoWindowClickListener windowClickListener = new OnInfoWindowClickListener()
	{
		@Override
		public void onInfoWindowClick(Marker marker)
		{
			final String markerTitle = marker.getTitle(); // Getting the marker title
			MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
			ParseUser currentUser = ParseUser.getCurrentUser(); // Getting the creating user
			
			if (!markersArray.getVertexByName(markerTitle).isMarker()) // If the marker is mid vertex show only title
				return;
			
			imageDiaglog= new Dialog(thisActivity, R.style.PauseDialog);
			imageDiaglog.setTitle(marker.getTitle());
			imageDiaglog.setContentView(R.layout.events_list_view);
			
			Button addEvenetBtn = (Button) imageDiaglog.findViewById(R.id.add_event_btn); // Binding the add event button
			addEvenetBtn.setOnClickListener(new OnClickListener() // Click listener for the "add event" button
			{
				@Override
				public void onClick(View v)
				{
					imageDiaglog.dismiss();
					Intent intent = new Intent(thisActivity.getApplicationContext(), AddEventActivity.class);
					Bundle bundle = new Bundle();
					bundle.putString("markerName", markerTitle);
					intent.putExtras(bundle);
					thisActivity.startActivity(intent);
				}
			});
			
			Button takeMeThere = (Button)imageDiaglog.findViewById(R.id.take_me_there); // Binding the take me there button for creating navigation to some point
			takeMeThere.setOnClickListener(new OnClickListener() // Click listener for the "take me there" button
			{
				@Override
				public void onClick(View v)
				{
					if (navigationHandler.isNavigating()) // If the application is already in navigation mode
					{
						imageDiaglog.dismiss(); // Closing the dialog if navigation is selected
						askForNewNavigation(markerTitle);
						return;
					}
					
					locationHandler.findCurrentLocation(null, markerTitle);
					imageDiaglog.dismiss(); // Closing the dialog if navigation is selected
				}
			});
			
			listAdapter = new ListAdapter(thisActivity, markersArray.getEventListByName(markerTitle), currentUser); // Setting the marker list adapter with his own event list
			
			ListView listView = (ListView) imageDiaglog.findViewById(R.id.listView1); // Binding the list view
			listView.setAdapter(listAdapter); // Setting the list view adapter
			
			markersArray.checkForNewMarkerEvents(thisActivity, markerTitle, listAdapter, imageDiaglog); // Check for new events
		}
	};
	
	/**
	 * Adapter for the markers info window
	 */
	private InfoWindowAdapter adapter = new InfoWindowAdapter()
	{
		@Override
		public View getInfoWindow(Marker arg0)
		{
			return null;
		}
		
		@Override
		public View getInfoContents(Marker marker)
		{
			MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
			byte[] markerImage = markersArray.getMarkerImage(marker.getTitle());
			
			if (!navigationHandler.isNavigating())
			{
				markersArray.setAllMarkersIconToDefault(); // Setting all markers icons back to default
				locationHandler.clearAllPolylines(); // Remove all the polylines from the map
			}
			
			View v = thisActivity.getLayoutInflater().inflate(R.layout.marker_layout, null);
			
			// Getting reference to the TextView to set title
			TextView note = (TextView) v.findViewById(R.id.title);
			note.setText(marker.getTitle());
			
			// Setting the marker image
			ImageView img = (ImageView) v.findViewById(R.id.image_place);
			
			if (markersArray.getVertexByName(marker.getTitle()).isMarker()) // Checking if the marker is a mid vertex, if yes don't show the image
				img.setImageBitmap(BitmapFactory.decodeByteArray(markerImage, 0, markerImage.length));
				//img.setImageResource(R.drawable.building);
			else
				img.setVisibility(View.INVISIBLE);
			
			return v;
		}
	};
	
	/**
	 * Async task for downloading the markers images
	 */
	private class GetMarkersImagesFromParse extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			progressDialog.dismiss(); // Close the progress dialog
			progressDialog = ProgressDialog.show(thisActivity, "Getting Markers Images", "Downloading...", false); // Show the user progress dialog while downloading the markers images
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
			
			for (int i=0 ; i<markers.size() ; ++i)
			{
				try
				{
					ParseFile markerImage = markers.get(i).getParseFile("image");
					byte[] image = markerImage.getData(); // Get the marker image from parse
					markersArray.setMarkerImage(markers.get(i).getString("title"), image); // Setting the marker image when download from parse finish
				}
				catch (ParseException e)
				{
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			super.onPostExecute(result);
			
			progressDialog.dismiss(); // Close the progress dialog
			pathHandler.buildPath(); // Continue to build the path using the handler
		}
	}
}
