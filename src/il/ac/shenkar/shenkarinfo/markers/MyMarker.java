package il.ac.shenkar.shenkarinfo.markers;

import com.google.android.gms.maps.model.Marker;

import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.events.EventListArray;

public class MyMarker
{
	private Vertex vertex;
	private Marker marker;
	private EventListArray eventArray;
	private byte[] image;
	
	public MyMarker(Vertex vertex, Marker marker)
	{
		super();
		this.vertex = vertex;
		this.marker = marker;
	}
	
	public MyMarker(Vertex vertex, Marker marker, byte[] image)
	{
		super();
		this.vertex = vertex;
		this.marker = marker;
		this.image = image;
	}
	
	public EventListArray getEventArray()
	{
		return eventArray;
	}

	public void setEventArray(EventListArray eventArray)
	{
		this.eventArray = eventArray;
	}

	public Vertex getVertex()
	{
		return vertex;
	}
	
	public void setVertex(Vertex vertex)
	{
		this.vertex = vertex;
	}
	
	public Marker getMarker()
	{
		return marker;
	}
	
	public void setMarker(Marker marker)
	{
		this.marker = marker;
	}

	public byte[] getImage()
	{
		return image;
	}

	public void setImage(byte[] image)
	{
		this.image = image;
	}
}
