package il.ac.shenkar.shenkarinfo.markers;

import il.ac.shenkar.shenkarinfo.ListAdapter;
import il.ac.shenkar.shenkarinfo.R;
import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.events.Event;
import il.ac.shenkar.shenkarinfo.events.EventListArray;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class MyMarkerArray
{
	private ProgressDialog progressDialog;
	private static MyMarkerArray markerArrayInstance;
	private Map<String,MyMarker> markersArray;
	public enum Colors {Red, Blue};
	
	private MyMarkerArray()
	{
		markersArray = new HashMap<String, MyMarker>();
	}
	
	public static MyMarkerArray getInstance()
	{
		if (markerArrayInstance == null)
			markerArrayInstance = new MyMarkerArray();
		return markerArrayInstance;
	}
	
	/**
	 * Adding new marker to the array, if the event list is null then the marker is a mid vertex and does not contain any events
	 */
	public void addMarker(String name, Vertex vertex, Marker marker, EventListArray eventList)
	{
		MyMarker newMarker = new MyMarker(vertex, marker);
		if (eventList != null)
			newMarker.setEventArray(eventList);
		markersArray.put(name, newMarker);
	}
	
	/**
	 * Getting a vertex by the name of the marker
	 */
	public Vertex getVertexByName(String name)
	{
		return markersArray.get(name).getVertex();
	}
	
	/**
	 * Getting a marker by the name of the marker
	 */
	public Marker getMarkerByName(String name)
	{
		return markersArray.get(name).getMarker();
	}
	
	/**
	 * Sets new marker icon according to his name
	 */
	public void setNewMarkerIconByName(String name, BitmapDescriptor icon)
	{
		markersArray.get(name).getMarker().setIcon(icon);
	}
	
	/**
	 * Sets all the markers on the map to their default (black) icon
	 */
	public void setAllMarkersIconToDefault()
	{
		for (MyMarker marker : markersArray.values())
		{
			if (marker.getVertex().isMarker())
				marker.getMarker().setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
			else
				marker.getMarker().setIcon(BitmapDescriptorFactory.fromResource(R.drawable.star_marker));
		}
	}
	
	/**
	 * Get all google markers in the map
	 */
	public List<Marker> getAllMarkers()
	{
		List<Marker> markerList = new ArrayList<Marker>();
		
		for (MyMarker marker : markersArray.values())
			markerList.add(marker.getMarker());
		
		return markerList;
	}
	
	/**
	 * Get all the vertexes in the map
	 */
	public List<Vertex> getAllVertexes()
	{
		List<Vertex> vertexList = new ArrayList<Vertex>();
		
		for (MyMarker marker : markersArray.values())
			vertexList.add(marker.getVertex());
		
		return vertexList;
	}
	
	/**
	 * Get all the vertexes names on the map
	 */
	public List<String> getAllVertexesNames()
	{
		List<String> vertexList = new ArrayList<String>();
		
		for (MyMarker marker : markersArray.values())
			vertexList.add(marker.getVertex().toString());
		
		return vertexList;
	}
	
	/**
	 * Receive a list of vertexes and change their icon to red, for marking a path
	 */
	public void setMarkersInPathToRed(List<Vertex> path)
	{
		for (Vertex vertex: path)
		{
			if (vertex.isMarker())
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_pin));
			else
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_star));
		}
	}
	
	/**
	 * Gets the marker events list
	 */
	public EventListArray getEventListByName(String name)
	{
		return markersArray.get(name).getEventArray();
	}
	
	/**
	 * Setting new icon for the given vertex in the given color
	 */
	public void setVertexNewIcon(Vertex vertex, Colors color)
	{
		switch(color)
		{
		case Blue:
			if (vertex.isMarker())
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.blue_pin));
			else
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.blue_star));
			break;
			
		case Red:
			if (vertex.isMarker())
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_pin));
			else
				getMarkerByName(vertex.getName()).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_star));
			break;
			
		}
	}
	
	/**
	 * This method is important for checking new events that were added by other users while the current user is online
	 */
	public void checkForNewMarkerEvents(Activity thisActivity, String markerName, final ListAdapter listAdapter, final Dialog imageDialog)
	{
		progressDialog = ProgressDialog.show(thisActivity, "Checking For New Events", "Checking...", false); // Show the user progress dialog while checking for new events
		List<String> eventsObjectIds = new ArrayList<String>();
		final EventListArray currentEventsList = getEventListByName(markerName); // Getting the marker from the singleton array
		
		for (int i = 0 ; i<currentEventsList.getSize(); ++i) // Get all the current events in the array
			eventsObjectIds.add(currentEventsList.getEvent(i).getId());
		
		// Create new query which get from parse only the events which doesn't exist in offline events array
		ParseQuery<ParseObject> getNewEventsQuery = ParseQuery.getQuery("Event");
		getNewEventsQuery.whereEqualTo("markerName", markerName);
		getNewEventsQuery.whereNotContainedIn("objectId", eventsObjectIds);

		getNewEventsQuery.findInBackground(new FindCallback<ParseObject>()
		{
			@Override
			public void done(List<ParseObject> newEvents, ParseException e)
			{
				Calendar eventDate = new GregorianCalendar();
				
				// If no new events just return 
				if (newEvents.isEmpty())
				{
					progressDialog.dismiss(); // Dismiss the progress dialog
					imageDialog.show(); // Show the image dialog of the marker
					return;
				}
				
				// Create new Event for each new events from the cloud and add it to the currentEventsList
				for (int i=0 ; i<newEvents.size() ; ++i)
				{
					// Create new event
					eventDate.setTime(newEvents.get(i).getDate("date")); // Convert the parse date to Calendar
					Event newEvent = new Event(newEvents.get(i).getObjectId(),
												newEvents.get(i).getString("title"),
												newEvents.get(i).getString("description"),
												eventDate,
												newEvents.get(i).getString("type"));
					newEvent.setOwner(newEvents.get(i).getString("owner"));
					newEvent.setMarkerName(newEvents.get(i).getString("markerName"));
					currentEventsList.addEventToArray(newEvent); // Add the newly created event to the array
				}
				progressDialog.dismiss(); // Dismiss the progress dialog
				listAdapter.notifyDataSetChanged(); // Refresh the listview
				imageDialog.show(); // Show the image dialog of the marker
			}
		});
	}
	
	public void setMarkerImage(String name, byte[] image)
	{
		markersArray.get(name).setImage(image);
	}
	
	public byte[] getMarkerImage(String name)
	{
		return markersArray.get(name).getImage();
	}
}
