package il.ac.shenkar.shenkarinfo.location;

import java.util.LinkedList;
import java.util.List;
import com.google.android.gms.maps.model.Polyline;

import il.ac.shenkar.shenkarinfo.R;
import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray.Colors;
import il.ac.shenkar.shenkarinfo.sound.RecorderThreadService;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class NavigationHandler
{
	private Activity thisActivity;
	private MyMarkerArray markersArray;
	private Intent serviceIntent;
	private LinkedList<Vertex> path;
	private List<Polyline> polylineArray;
	private boolean isNavigating;
	private TextView instructions;
	
	public NavigationHandler(Activity thisActivity)
	{
		this.thisActivity = thisActivity;
		this.markersArray = MyMarkerArray.getInstance();
		instructions = (TextView) thisActivity.findViewById(R.id.instructions);
	}

	/**
	 * Method for starting a navigation.
	 * Starting the recording service and start listening, as well as opening a small window for showing the 
	 * user instructions
	 */
	public void startNavigate()
	{
		isNavigating = true;
		
		instructions.setText("Waiting For Frequency Signal");
		instructions.setBackgroundColor(Color.GRAY); // Sets the instructions background
		instructions.setAnimation(AnimationUtils.loadAnimation(thisActivity, R.anim.bounce));
		instructions.setVisibility(View.VISIBLE); // Sets the instructions visible to the user
		instructions.bringToFront(); // Bring the instructions to the front
		
		serviceIntent = new Intent(thisActivity.getApplicationContext(), RecorderThreadService.class);
		serviceIntent.putExtra("handler", new Messenger(handler)); // Adding the handler to the service
		thisActivity.startService(serviceIntent); // Starting the recorder thread
	}
	
	/**
	 * Method for finishing navigation by stopping the recording service and closing the instruction window
	 */
	public void stopNavigation()
	{
		instructions.setAnimation(AnimationUtils.loadAnimation(thisActivity, R.anim.slide_up));
		
		markersArray.setAllMarkersIconToDefault(); // Setting all icons back to default (black)
		clearAllPolylines(); // Clearing the polylines on the map
		isNavigating = false;
		thisActivity.stopService(serviceIntent); // Stop the recording service
		
		instructions.setVisibility(View.INVISIBLE); // Setting the instructions invisible
	}
	
	/**
	 * Method for removing all the polylines from the map
	 */
	public void clearAllPolylines()
	{
		for (Polyline polyline: polylineArray)
			polyline.remove();
		polylineArray.clear();
	}
	
	/**
	 * Handler for receiving the frequency from the service
	 */
	private Handler handler = new Handler(new Handler.Callback()
	{
		@Override
		public boolean handleMessage(Message msg)
		{
			double freqResult = msg.getData().getDouble("freqResult"); // Getting the frequency from the recorder service
			
			for (Vertex vertex: markersArray.getAllVertexes()) // Going over all the vertexes and checking which one has the frequency within the range +-100Hz from the frequency result
				if (vertex.getFrequency() < freqResult+100 && vertex.getFrequency() > freqResult-100 && path.contains(vertex))
				{
					if (vertex.getName().equals(path.getLast().getName())) // If the user reached his destination
					{
						stopNavigation(); // Stopping the recording service
						
						Toast.makeText(thisActivity, "You've reached the " + vertex.getName() +" :)", Toast.LENGTH_SHORT).show();
						return false;
					}
					markersArray.setMarkersInPathToRed(path);
					markersArray.setVertexNewIcon(vertex, Colors.Blue);
					instructions.setText("You're In: " + path.get(path.indexOf(vertex)).toString() + "\n" +
											"Next stop: " + path.get(path.indexOf(vertex)+1).toString());
				}
			
			return true;
		}
	});

	public void setPath(LinkedList<Vertex> path)
	{
		this.path = path;
	}

	public void setPolylineArray(List<Polyline> polylineArray)
	{
		this.polylineArray = polylineArray;
	}

	public boolean isNavigating()
	{
		return isNavigating;
	}
}
