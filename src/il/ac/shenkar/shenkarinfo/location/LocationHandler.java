package il.ac.shenkar.shenkarinfo.location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import il.ac.shenkar.shenkarinfo.PathHandler;
import il.ac.shenkar.shenkarinfo.R;
import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray.Colors;
import il.ac.shenkar.shenkarinfo.sound.RecorderThreadService;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class LocationHandler
{
	private Activity thisActivity;
	private Intent serviceIntent;
	private ProgressDialog progressDialog;
	private String currentLocation;
	private GoogleMap map;
	private PathHandler pathHandler;
	private MyMarkerArray markersArray;
	private long timerDelay;
	private NavigationHandler navigationHandler;
	private static final float MIN_ZOOM = 18;
	
	private List<String> locationResults;
	private List<String> listOfVertex;
	private List<Polyline> polylineArray;
	
	public LocationHandler(Activity thisActivity, GoogleMap map, PathHandler pathHandler, NavigationHandler navigationHandler)
	{
		currentLocation = null;
		markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
		this.thisActivity = thisActivity;
		this.map = map;
		this.pathHandler = pathHandler;
		this.navigationHandler = navigationHandler;
		this.polylineArray = new ArrayList<Polyline>();
	}
	
	/**
	 * Method for removing all the polylines from the map
	 */
	public void clearAllPolylines()
	{
		for (Polyline polyline: polylineArray)
			polyline.remove();
		polylineArray.clear();
	}

	/**
	 * Method for getting current location.
	 * If the method receives a destination it will calculate the path between the current position and the destination
	 */
	public void findCurrentLocation(final String source, final String destination)
	{
		listOfVertex = new ArrayList<String>();
		locationResults = new ArrayList<String>();

		if (source == null) // If the source is not known start the recording service to find it out
		{	
			progressDialog = ProgressDialog.show(thisActivity, "Finding Your Location", "Retrieving Location In 3", false); // Let the user know the location is been searched
			timerDelay = 4000;
			serviceIntent = new Intent(thisActivity.getApplicationContext(), RecorderThreadService.class);
			serviceIntent.putExtra("handler", new Messenger(handler)); // Adding the handler to the service
			thisActivity.startService(serviceIntent); // Starting the recorder thread
		}
		else // Else the source is known just build the path
		{
			timerDelay = 0;
		}
		
		new CountDownTimer(timerDelay, 1000) // Count down "timerDelay" seconds until stopping the service
		{
			public void onTick(long millisUntilFinished)
			{
				progressDialog.setMessage("Retrieving Location In " + String.valueOf(millisUntilFinished / 1000)); // Updating the user with the remaining time for location determining
			}
			
			public void onFinish()
			{
				MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
				markersArray.setAllMarkersIconToDefault();
				
				progressDialog.dismiss(); // Close the progress dialog
				thisActivity.stopService(serviceIntent); // Stop the recording service

				if (source == null)
					currentLocation = getHighestNumberOfShows(); // Get the location which was found the most
				else
					currentLocation = source;
				
				if (currentLocation == null) // If the current location could not be found
				{
					askForLocationFromUserAlert(destination);
					return;
				}
				
				if (destination == null) // If no destination was given just get the current location
				{
					// Checking if the marker type to change to the correct icon
					markersArray.setVertexNewIcon(markersArray.getVertexByName(currentLocation), Colors.Red);
					map.moveCamera(CameraUpdateFactory.newLatLngZoom(markersArray.getMarkerByName(currentLocation).getPosition(), MIN_ZOOM));
					
					Toast.makeText(thisActivity, "You're in " + currentLocation, Toast.LENGTH_SHORT).show();
				}
				else // Else build path for navigation
				{
					if (currentLocation.equals(destination)) // If the user is already in the given destination
					{
						Toast.makeText(thisActivity, "You are already there :)", Toast.LENGTH_SHORT).show();
						return;
					}
					
					LinkedList<Vertex> path = pathHandler.getPath(currentLocation, destination); // Get the path between the current location to destination
					
					for (int i=0 ; i<path.size()-1 ; ++i)
					{
						// Checking the vertexes along the way to red
						markersArray.setVertexNewIcon(markersArray.getVertexByName(path.get(i).toString()), Colors.Red);
						
						// If this is the first vertex mark it in blue
						if (i == 0)
							markersArray.setVertexNewIcon(markersArray.getVertexByName(path.get(i).toString()), Colors.Blue);
						
						LatLng source = markersArray.getMarkerByName(path.get(i).toString()).getPosition();
						LatLng dest = markersArray.getMarkerByName(path.get(i+1).toString()).getPosition();

						Polyline newPolyline = map.addPolyline(new PolylineOptions().add(source, dest).width(4).color(Color.RED)); // Creating new polyline
						polylineArray.add(newPolyline); // Adding the new polyline to an array
						
						if (i+2 == path.size()) // If it's that last marker change his icon as well
							markersArray.setNewMarkerIconByName(path.get(i+1).toString(), BitmapDescriptorFactory.fromResource(R.drawable.red_pin));
					}
					
					navigationHandler.setPath(path);
					navigationHandler.setPolylineArray(polylineArray);
					navigationHandler.startNavigate();
				}
			}
		}.start();
	}
	
	/**
	 * Method for getting the vertex with highest number of times retrieved, This method is needed
	 * in case the frequency is not stable and more than one place is found.
	 */
	private String getHighestNumberOfShows()
	{
		String location = null;
		
		if (locationResults.isEmpty())
			return null;
		
		if (locationResults.size() == 1)
			return locationResults.get(0);

		for (int i=0 , temp=0 ; locationResults.size()>i ; ++i)
			if (Collections.frequency(listOfVertex, locationResults.get(i)) > temp)
			{
				temp = Collections.frequency(listOfVertex, locationResults.get(i));
				location = locationResults.get(i);
			}
		
		return location;
	}
	
	/**
	 * Shows Alert dialog to the user in case the current location was not found and asks the user to manually input the location
	 * If the user input this data the location will be marked on the map or in case the user initially wanted to navigate to a certain point
	 * the findCurrentLocation method will be called again with the source the user picked
	 */
	private void askForLocationFromUserAlert(final String destination)
	{
		LayoutInflater li = LayoutInflater.from(thisActivity);
		View promptsView = li.inflate(R.layout.location_not_found_dialog, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(thisActivity);
        alertDialogBuilder.setView(promptsView);
        
        alertDialogBuilder.setTitle("Current Location Not Found");
        
        final Spinner spinner= (Spinner) promptsView.findViewById(R.id.location_not_found_spinner);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(thisActivity, android.R.layout.simple_spinner_item, markersArray.getAllVertexesNames());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				findCurrentLocation(spinner.getSelectedItem().toString(), destination); // If the user picked a location call the findCurrentLocation method again
			}
		});
        
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				return; // If the user does not want to pick a location just return
			}
		});
        
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
	}
	
	/**
	 * Handler for receiving the frequency from the service
	 */
	private Handler handler = new Handler(new Handler.Callback()
	{
		@Override
		public boolean handleMessage(Message msg)
		{
			double freqResult = msg.getData().getDouble("freqResult"); // Getting the frequency from the recorder service
			
			for (Vertex vertex: markersArray.getAllVertexes()) // Going over all the vertexes and checking which one has the frequency within the range +-100Hz from the frequency result
				if (vertex.getFrequency() < freqResult+100 && vertex.getFrequency() > freqResult-100)
				{
					if (!locationResults.contains(vertex.getName()))
						locationResults.add(vertex.getName());
					listOfVertex.add(vertex.getName());
				}
			
			return true;
		}
	});
}
