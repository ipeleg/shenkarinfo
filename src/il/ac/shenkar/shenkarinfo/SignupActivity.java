package il.ac.shenkar.shenkarinfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignupActivity extends Activity
{
	private ProgressDialog progressDialog;
	
	private Button signupBtn;
	private EditText usernameField;
	private EditText passwordField;
	private EditText confirmPasswordField;
	private EditText emailField;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		ParseAnalytics.trackAppOpened(getIntent());
		
		setContentView(R.layout.sign_up); // Setting the activity layout
		
		// Making all the view components binding
		signupBtn = (Button) findViewById(R.id.signup_btn);
		usernameField = (EditText) findViewById(R.id.username_signup);
		passwordField = (EditText) findViewById(R.id.password_signup);
		confirmPasswordField = (EditText) findViewById(R.id.confirm_password_sigup);
		emailField = (EditText) findViewById(R.id.email_sigup);
		
		signupBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				progressDialog = ProgressDialog.show(SignupActivity.this, "Signing Up", "Loading...", false);
				
				// If one of the fields was not filled
				if (usernameField.getText().toString().equals("") || passwordField.getText().toString().equals("") || emailField.getText().toString().equals(""))
				{
					Toast.makeText(getApplicationContext(), "You must fill all fields to signup!", Toast.LENGTH_SHORT).show();
					progressDialog.dismiss();
					return;
				}
				
				// If the passwords are not equal
				if (!passwordField.getText().toString().equals(confirmPasswordField.getText().toString()))
				{
					Toast.makeText(getApplicationContext(), "Passwords are not equal!", Toast.LENGTH_SHORT).show();
					progressDialog.dismiss();
					return;
				}
				
				// Creating a new user
				ParseUser user = new ParseUser();
				user.setUsername(usernameField.getText().toString());
				user.setPassword(passwordField.getText().toString());
				user.setEmail(emailField.getText().toString());
				
				// Signup the user in background thread
				user.signUpInBackground(new SignUpCallback()
				{
					@Override
					public void done(ParseException e)
					{
						if (e == null) // If signup succeeded go to MainActivity
						{
							progressDialog.dismiss();
							startActivity(new Intent(getApplicationContext(),MainActivity.class));
							finish();
						}							
						else // Else dismiss progress dialog and notify the user
						{
							progressDialog.dismiss();
							Toast.makeText(getApplicationContext(), "Signup Failed.", Toast.LENGTH_SHORT).show();
							Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
						}
					}
				});
			}
		});
	}
}
