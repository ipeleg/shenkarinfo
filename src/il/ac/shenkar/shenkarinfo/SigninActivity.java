package il.ac.shenkar.shenkarinfo;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;

public class SigninActivity extends Activity
{
	private ProgressDialog progressDialog;
	
	private TextView firstTimeTitle;
	private Button signinBtn;
	
	private EditText usernameField;
	private EditText passwordField;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sign_in); // Setting the activity layout
		
		// First check for Internet connectivity
		if (!isNetworkAvailable())
		{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			alertDialogBuilder.setTitle("No Internet")
								.setMessage("This application requires an active internet connection.\n" +
										"Please connect the nearest Wifi or use your 3G service.")
								.setCancelable(false)
								.setNeutralButton("OK", new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog, int which)
									{
										finish();
									}
								})
								.create()
								.show();
			return;
		}
		
		ParseAnalytics.trackAppOpened(getIntent());
		
		ParseUser currentUser = ParseUser.getCurrentUser(); // Checking if the user is already connected
		if (currentUser != null)
		{
			Toast.makeText(getApplicationContext(), "Welcome Back " + currentUser.getUsername(), Toast.LENGTH_SHORT).show();
			startActivity(new Intent(getApplicationContext(), MainActivity.class));
			finish();
		}
		
		// Making all the view components binding
		firstTimeTitle = (TextView) findViewById(R.id.first_time_title);
		signinBtn = (Button) findViewById(R.id.signin_btn);
		usernameField = (EditText) findViewById(R.id.username_signin);
		passwordField = (EditText) findViewById(R.id.password_signin);
		
		// Button listener for the signin button
		signinBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				progressDialog = ProgressDialog.show(SigninActivity.this, "Loging In", "Loading...", false); // Show the user progress dialog while logging
				
				ParseUser.logInInBackground(usernameField.getText().toString(), passwordField.getText().toString(), new LogInCallback()
				{
					@Override
					public void done(ParseUser user, ParseException e)
					{
						 if (user != null) // If user successfully logged in go to MainActivity
						 {
							 progressDialog.dismiss();
							 startActivity(new Intent(getApplicationContext(),MainActivity.class));
							 finish();
						 }
						 else // Else dismiss progress dialog and notify the user
						 {
							 progressDialog.dismiss();
							 Toast.makeText(getApplicationContext(), "Login Failed.", Toast.LENGTH_SHORT).show();
						 }
					}
				});
			}
		});
		
		SpannableString spannableString = new SpannableString("First Time? Signup Now."); // Creating a SpanString
		ClickableSpan clickableSpan = new ClickableSpan() // Setting the onClickListener for the SpanString
		{
            @Override
            public void onClick(View textView)
            {
                startActivity(new Intent(getApplicationContext(), SignupActivity.class));
            }
        };
        spannableString.setSpan(clickableSpan, 12, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // Setting the span area within the string
        
        firstTimeTitle.setText(spannableString); // Setting the SpanString as the title text
        firstTimeTitle.setMovementMethod(LinkMovementMethod.getInstance()); // Allowing the TextView to be clickable
	}
	
	public boolean isNetworkAvailable()
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
	}
}
