package il.ac.shenkar.shenkarinfo.sound;

import java.util.ArrayList;

import android.app.IntentService;
import android.content.Intent;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.AudioFormat;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class RecorderThreadService extends IntentService
{
	private RealDoubleFFT transformer;
	private AudioRecord recorder;
	
	private short audioData[];
	private double toTransform[];
	
	private int freqThreshold = 18000; // Variable to set the minimum frequency to be filtered (Hertz)
	private double amplitudeThreshold = 9 ;  // variable to set the minimum volume of frequency range (Db spl)
	
	private int bufferSize;
	private int counter;
	private double ampSum;
	private double ampAvg;
	private static double currentFreq;  // Variable to hold the current frequency
	private boolean recording; // Variable to start or stop recording
	
	private static final int numOfRecords = 30; // Number of total records needed before sending back result
	private int numOfRecordsIndex = 0;
	private ArrayList<Double> recordsArray;
	
	private Messenger messenger;
	
	public RecorderThreadService()
	{
		super("RecorderThread");
	}
	
	@Override
	protected void onHandleIntent(Intent intent)
	{
		double maxMagnitude;
		int maxIndex;
		
		System.out.println("Record Started");

		recordsArray = new ArrayList<Double>();
		
		bufferSize = AudioRecord.getMinBufferSize(44100,
				AudioFormat.CHANNEL_IN_STEREO,
				AudioFormat.ENCODING_PCM_16BIT) * 3; // get the buffer size to use with this audio record

		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 44100,
				AudioFormat.CHANNEL_IN_STEREO,
				AudioFormat.ENCODING_PCM_16BIT, bufferSize); // instantiate the  AudioRecorder

		recording = true; // variable to use start or stop recording
		audioData = new short[bufferSize]; // short array that PCM data is put into.
		toTransform = new double[bufferSize];
		transformer = new RealDoubleFFT(bufferSize);
		messenger = (Messenger) intent.getExtras().get("handler"); 

		while (recording)
		{
			if(counter>50)
			{
				counter=0;
				ampSum=0;
			}
			
			maxIndex = -1;
			maxMagnitude = 0;
			
			if (recorder.getState() == android.media.AudioRecord.STATE_INITIALIZED) // check to see if the recorder has initialized yet.
				if (recorder.getRecordingState() == android.media.AudioRecord.RECORDSTATE_STOPPED)
					recorder.startRecording(); // check to see if the Recorder has stopped or is not recording, and make it record.
				else
				{
					int bufferReadResult = recorder.read(audioData, 0, bufferSize); // Read the PCM audio data into the audioData array

					for (int i = 0; i < bufferSize && i < bufferReadResult; i++)
                        toTransform[i] = (double) audioData[i] / 32768.0; // Signed 16 bit
					
					transformer.ft(toTransform);
					
					for (int i = 0; i < bufferSize/2-1; i++)
						if(i * 44100 / bufferSize > freqThreshold && (toTransform[i] > amplitudeThreshold)  )
						{
							// Increment the counter to calculate the amplitude average 
							counter++;
							
							// Setting the max amplitude peak variable  
							maxMagnitude = toTransform[i];
							
							ampSum += maxMagnitude;
							maxIndex = i;
							currentFreq = maxIndex * 44100 / bufferSize;
							System.out.println("Frequency: " + currentFreq);
							ampAvg=ampSum/counter;			                
			                recordsArray.add(currentFreq); // Adding the frequency to the array
			                ++numOfRecordsIndex; // Increment the number of records so far
			                
			                if (numOfRecordsIndex == numOfRecords) // If we reached 30 records send the average to the main activity
			                {
			                	double sum = 0;
			                	numOfRecordsIndex = 0; // Set the number of records index to zero
			                	
			                	for (Double freq: recordsArray) // Calculating the sum of all frequencies for calculating the average
			                		sum += freq;
			                	
			                	System.out.println("Freq: " + sum/numOfRecords + " Amplitude RMS : " + ampAvg + "   Amp: " + maxMagnitude  );
			                	
			                	recordsArray.clear(); // Clearing the array from all values
			                	
								Message msg = Message.obtain(); 
				                Bundle data = new Bundle();
			                	
				                data.putDouble("freqResult", sum/numOfRecords); // Setting the average of frequencies as a result
				                data.putInt("ampResult", (int) ampAvg);
				                data.putInt("counter", counter);
				                msg.setData(data);
				                
				                try
				                { 
				                    messenger.send(msg); // Sending the average frequency of the last "numOfRecords" records
				                }
				                catch (RemoteException e)
				                {
				                    e.printStackTrace(); 
				                }
			                }
						}
				}
			
			audioData = new short[bufferSize]; // short array that PCM data is put into.
			toTransform = new double[bufferSize];
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		recording = false;
		if (recorder.getState() == android.media.AudioRecord.RECORDSTATE_RECORDING)
			recorder.stop(); // Stop the recorder before ending the thread
		recorder.release(); // Release the recorders resources
		recorder = null; // Set the recorder to be garbage collected
		System.out.println("Record Stopped");
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}
}