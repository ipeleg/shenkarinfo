package il.ac.shenkar.shenkarinfo.events;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import android.app.ProgressDialog;
import android.content.Context;

public class EventListArray
{
	private ArrayList<Event> eventsArray;

	public EventListArray(Context context, final String markerName)
	{
		eventsArray = new ArrayList<Event>();
		ProgressDialog progressDialog = ProgressDialog.show(context, "Getting Events", "Downloading...", false); // Show the user progress dialog while downloading the events
		ParseQuery<ParseObject> getEventsQuery = ParseQuery.getQuery("Event");
		getEventsQuery.whereEqualTo("markerName", markerName); // Get only the events for the specific marker
		getEventsQuery.findInBackground(new  FindCallback<ParseObject>()
		{
			@Override
			public void done(List<ParseObject> events, ParseException arg1)
			{
				Calendar eventDate = new GregorianCalendar();
				
				for (int i=0 ; i<events.size(); ++i)
				{
					eventDate.setTime(events.get(i).getDate("date")); // Convert the parse date to Calendar
					Event newEvent = new Event(events.get(i).getObjectId(),
												events.get(i).getString("title"),
												events.get(i).getString("description"), 
												eventDate,
												events.get(i).getString("type"));
					newEvent.setOwner(events.get(i).getString("owner"));
					newEvent.setMarkerName(events.get(i).getString("markerName"));
					eventsArray.add(newEvent);
				}
			}
		});
		progressDialog.dismiss();
	}
	
	public void addEventToArray(Event newEvent)
	{
		eventsArray.add(0, newEvent); // Adding new element to the first place in the array
	}

	public void addEventToCloud(final Event newEvent)
	{
		eventsArray.add(0, newEvent); // Adding new element to the first place in the array
		
		final ParseObject parseEvent = new ParseObject("Event");
		parseEvent.put("title", newEvent.getTitle());
		parseEvent.put("description", newEvent.getDescription());
		parseEvent.put("date", newEvent.getDatetime().getTime());
		parseEvent.put("type", newEvent.getType());
		parseEvent.put("markerName", newEvent.getMarkerName());
		parseEvent.put("owner",newEvent.getOwner());
		parseEvent.saveInBackground(new SaveCallback()
		{
			@Override
			public void done(ParseException e)
			{
				newEvent.setId(parseEvent.getObjectId());
			}
		});
		parseEvent.saveInBackground();
	}

	public void removeEvent(int position)
	{
		Event event2Del = eventsArray.get(position);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
		query.getInBackground(event2Del.getId(), new GetCallback<ParseObject>()
		{
			@Override
			public void done(ParseObject parseEvent, ParseException e)
			{
				if (e == null)
				{
					parseEvent.deleteInBackground();
					parseEvent.saveInBackground();
				}

			}
		});
		eventsArray.remove(position);
	}

	public int getSize()
	{
		return eventsArray.size();
	}

	public Event getEvent(int position)
	{
		return eventsArray.get(position);
	}

	public boolean updateEvent(Event event, String id)
	{
		final Event scooleEvent = event;
		for (int i = 0; i < eventsArray.size(); ++i)
		{
			if (eventsArray.get(i).getId() == id)
			{
				eventsArray.set(i, event);
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
				// Retrieve the object by id
				query.getInBackground(id, new GetCallback<ParseObject>()
				{
					@Override
					public void done(ParseObject parseEvent, ParseException e)
					{
						if (e == null)
						{
							parseEvent.put("title", scooleEvent.getTitle());
							parseEvent.put("description",
									scooleEvent.getDescription());
							parseEvent.put("date", scooleEvent.getDatetime());
							parseEvent.put("type", scooleEvent.getType());
							parseEvent.saveInBackground();
						}

					}
				});
				return true;
			}
		}
		return false;

	}
}
