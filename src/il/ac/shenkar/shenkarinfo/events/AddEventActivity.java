package il.ac.shenkar.shenkarinfo.events;

import il.ac.shenkar.shenkarinfo.R;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.parse.ParseUser;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AddEventActivity extends Activity
{
	private TextView startingOutput;
	private EditText title, description;
	private Button chooseBtn,submit;
    private Context context;
    private Event event;
    private Spinner spinner;
    private String type;
    private Calendar time;
    private Calendar mcurrentTime;
    
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addevent_activity); // Setting the activity layout
		context = this;
		startingOutput = (TextView) findViewById(R.id.start_at_text);
		title = (EditText)findViewById(R.id.addevent_title);
		description=(EditText) findViewById(R.id.addevent_edit_description);
		spinner =(Spinner) findViewById(R.id.spinner);
		List<String> list = new ArrayList<String>();
		list.add("Social");
		list.add("Academic");
		spinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				type = parent.getItemAtPosition(pos).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
				type = "Social";
			}
		});
        
        chooseBtn = (Button)findViewById(R.id.choose_btn);
        chooseBtn.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View arg0)
			{
				mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        startingOutput.setText( selectedHour + ":" + selectedMinute);
                        time = Calendar.getInstance(TimeZone.getDefault());
                        time.set(Calendar.HOUR_OF_DAY, selectedHour);
                        time.set(Calendar.MINUTE, selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
			}
		});
        
		submit = (Button) findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if ((title.getText().toString() != "") &&(description.getText().toString() != "")) 
				{
					ParseUser currentUser = ParseUser.getCurrentUser(); // Getting the creating user
					MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
					String markerName = getIntent().getExtras().getString("markerName");
					event = new Event(title.getText().toString(), description.getText().toString(), time, type);
					event.setMarkerName(markerName);
					event.setOwner(currentUser.getObjectId());
					markersArray.getEventListByName(markerName).addEventToCloud(event);
					Toast.makeText(getApplicationContext(), "Thanks for sharing!", Toast.LENGTH_SHORT).show();
					finish();
				} 
				else 
				{
					Toast.makeText(getApplicationContext(), "Error: Could not add the event, Please fill all fields.", Toast.LENGTH_SHORT).show();
				}				
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
