package il.ac.shenkar.shenkarinfo.events;

import java.util.Calendar;

public class Event
{
	private String id;
	private String title;
	private String description;
	private Calendar datetime;
	private String type;
	private String markerName;
	private String owner;
	
	public Event(String id,String title, String description, Calendar datetime,
			String type)
	{
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.datetime = datetime;
		this.type = type;
	}
	
	public Event(String title, String description, Calendar datetime,
			String type)
	{
		super();
		this.title = title;
		this.description = description;
		this.datetime = datetime;
		this.type = type;
	}
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Calendar getDatetime()
	{
		return datetime;
	}
	public void setDatetime(Calendar datetime)
	{
		this.datetime = datetime;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}

	public String getMarkerName()
	{
		return markerName;
	}

	public void setMarkerName(String markerName)
	{
		this.markerName = markerName;
	}

	public String getOwner()
	{
		return owner;
	}

	public void setOwner(String owner)
	{
		this.owner = owner;
	}
}
