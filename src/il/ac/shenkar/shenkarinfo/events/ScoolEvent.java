package il.ac.shenkar.shenkarinfo.events;

import java.util.Date;

public class ScoolEvent
{
	private String id;
	private String title;
	private String description;
	private Date datetime;
	private String type;
	private String markerName;
	
	public ScoolEvent(String id,String title, String description, Date datetime,
			String type)
	{
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.datetime = datetime;
		this.type = type;
	}
	
	public ScoolEvent(String title, String description, Date datetime,
			String type)
	{
		super();
		this.title = title;
		this.description = description;
		this.datetime = datetime;
		this.type = type;
	}
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public Date getDatetime()
	{
		return datetime;
	}
	public void setDatetime(Date datetime)
	{
		this.datetime = datetime;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}

	public String getMarkerName()
	{
		return markerName;
	}

	public void setMarkerName(String markerName)
	{
		this.markerName = markerName;
	}
}
