package il.ac.shenkar.shenkarinfo.dijkstra;

public class Vertex
{
	final private String id;
	final private String name;
	final private double frequency;
	final private boolean isMarker;

	public Vertex(String id, String name, double frequency, boolean isMarker)
	{
		this.id = id;
		this.name = name;
		this.frequency = frequency;
		this.isMarker = isMarker;
	}
	
	public boolean isMarker()
	{
		return isMarker;
	}

	public double getFrequency()
	{
		return frequency;
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return name;
	}
}
