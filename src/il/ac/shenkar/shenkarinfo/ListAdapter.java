package il.ac.shenkar.shenkarinfo;

import java.text.SimpleDateFormat;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import il.ac.shenkar.shenkarinfo.email.GMailSender;
import il.ac.shenkar.shenkarinfo.events.EventListArray;
import android.app.AlertDialog;
import android.content.*;
import android.os.AsyncTask;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;

public class ListAdapter extends BaseAdapter
{
	private EventListArray eventList;
	private LayoutInflater l_Inflater;
	private Context taskListContext;
	private ParseUser currentUser;
	
	public ListAdapter(Context context, EventListArray eventList, ParseUser currentUser)
	{
		this.eventList = eventList;
	    this.taskListContext = context;
	    this.currentUser = currentUser;
	    l_Inflater = LayoutInflater.from(context);
	}
	
	public int getCount()
	{
	    return eventList.getSize();
	}
	
	public Object getItem(int position)
	{
	    return eventList.getEvent(position);
	}
	
	public long getItemId(int position)
	{
	    return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent)
	{
	    ViewHolder holder;
	    final int index = position;
	    
	    if (convertView == null)
	    {
	        convertView = l_Inflater.inflate(R.layout.event_item, null);
	        holder = new ViewHolder();
	        
	        holder.eventTitle = (TextView) convertView.findViewById(R.id.ev_title);
	        holder.eventDescription = (TextView) convertView.findViewById(R.id.ev_description);
	        holder.delete = (Button) convertView.findViewById(R.id.delete_btn);
	        holder.eventTime = (TextView) convertView.findViewById(R.id.ev_time);
	        
	        convertView.setTag(holder);
	    }
	    else
	    {
	        holder = (ViewHolder) convertView.getTag();
	    }
	    
	    holder.eventTitle.setText(eventList.getEvent(position).getTitle());
	    holder.eventDescription.setText(eventList.getEvent(position).getDescription());
	    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    holder.eventTime.setText(simpleDateFormat.format(eventList.getEvent(position).getDatetime().getTime()));
	    
	    // ClickListener for the delete task button 
	    holder.delete.setOnClickListener(new OnClickListener() // OnClickListener to delete item from list 
	    {
	        public void onClick(View v) 
	        {
	        	// Checking if the current user is the owner of this event
	        	if (!eventList.getEvent(index).getOwner().equals(currentUser.getObjectId()))
	        	{
		            DialogInterface.OnClickListener notifyOwnerDialog = new DialogInterface.OnClickListener()
		            {
		                public void onClick(DialogInterface dialog, int which)
		                {
		                    switch (which)
		                    {
		                    	case DialogInterface.BUTTON_POSITIVE: // Send mail to the owner
		                    				new SendMailAsyncTask().execute(eventList.getEvent(index).getOwner()
		                    						, eventList.getEvent(index).getTitle()
		                    						, eventList.getEvent(index).getMarkerName()
		                    						, simpleDateFormat.format(eventList.getEvent(index).getDatetime().getTime()));
		                                    break;
		
		                    	case DialogInterface.BUTTON_NEGATIVE: // Do not send mail to the owner
		                    		break;
		                    }
		                }
		            };
		
		            AlertDialog.Builder builder = new AlertDialog.Builder(taskListContext);
		            builder.setTitle("Only owner can delete event")
		            		.setMessage("Would you like to notify the owner?")
		            		.setPositiveButton("Yes", notifyOwnerDialog)
		            		.setNegativeButton("No", notifyOwnerDialog)
		            		.show();
	        	}
	        	else
	        	{
		            DialogInterface.OnClickListener deleteEventDialog = new DialogInterface.OnClickListener()
		            {
		                public void onClick(DialogInterface dialog, int which)
		                {
		                    switch (which)
		                    {
		                    	case DialogInterface.BUTTON_POSITIVE: // Delete the task if yes is selected
		                                    eventList.removeEvent(index); // Removing the task from the array
		                                    notifyDataSetChanged(); // Refreshing the ListView
		                                    break;
		
		                    	case DialogInterface.BUTTON_NEGATIVE: // Do nothing if "no" is selected
		                    		break;
		                    }
		                }
		            };
		
		            AlertDialog.Builder builder = new AlertDialog.Builder(taskListContext);
		            builder.setMessage("Delete event? Are sure about that?")
		            				.setPositiveButton("Yes", deleteEventDialog)
		            				.setNegativeButton("No", deleteEventDialog)
		            				.show();
	        	}
	        }
	    });
	    
	    notifyDataSetChanged();
	    return convertView;
	}
	
	static class ViewHolder
	{
	    TextView eventTitle;
	    TextView eventDescription;
	    TextView eventTime;
	   
	    Button delete = null;
	}
	
	/**
	 * Async task for sending notification mail to the event owner in case another user tried to delete the event.
	 * The email will be sent from the company personal email shenkarinfo@gmail.com
	 * The async task will get the owner ID and will ask for his email in the background service
	 */
	class SendMailAsyncTask extends AsyncTask<String, Void, Boolean>
	{
		private String currentUserUsername;
		
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			currentUserUsername = currentUser.getUsername();
			Toast.makeText(taskListContext, "Preparing notification", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Boolean doInBackground(String... params)
		{
			try
			{
				// Getting the owner object
				ParseQuery<ParseUser> getOwnerEmailQuery = ParseQuery.getQuery("_User");
				ParseUser owner = getOwnerEmailQuery.get(params[0]);
				
				// Logging into the ShenkarInfo gmail account
				GMailSender sender = new GMailSender("shenkarinfo@gmail.com", "123456iH");
				// Creating the email and sending it
				sender.sendMail("Is your event in ShenkarInfo ended?",
						"Hello "+ owner.getUsername() +",\nYou're receiving this mail because "+ currentUserUsername +" wanted to notify you about the event \"" +  params[1] + "\"\n" +
								"in the: \"" + params[2] + "\" on the " + params[3].replace(" ", " at ") + ", Could it be over? let us know if so.\n\n\n" +
						"Thank you for visiting Shenkar College.\n" +
						"The ShenkarInfo Team :)",
						"shenkarinfo@gmail.com",
						owner.getEmail());
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
			
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			// Let the user know if notifying the event owner succeeded or not
			super.onPostExecute(result);
			if (result)
				Toast.makeText(taskListContext, "Owner was notified, Thanks :)", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(taskListContext, "Something was wrong, Email couldn't be sent.", Toast.LENGTH_SHORT).show();
		}
	}
}