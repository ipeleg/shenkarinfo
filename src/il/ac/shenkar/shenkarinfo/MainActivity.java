package il.ac.shenkar.shenkarinfo;

import il.ac.shenkar.shenkarinfo.location.LocationHandler;
import il.ac.shenkar.shenkarinfo.location.NavigationHandler;
import il.ac.shenkar.shenkarinfo.markers.MarkersHandler;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.parse.ParseAnalytics;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity
{
	private GoogleMap map;
	private MapFragment mapFragment;
	
	public static final double MAX_LONGITUDE = 34.805003;
    public static final double MIN_LONGITUDE = 34.800947;
    public static final double MAX_LATITUDE = 32.094267;
    public static final double MIN_LATITUDE = 32.085685;
	private static final float MIN_ZOOM = 18;
	
	private PathHandler pathHandler;
	private LocationHandler locationHandler;
	private MarkersHandler markersHandler;
	private NavigationHandler navigationHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main_activity); // Setting the activity layout
		ParseAnalytics.trackAppOpened(getIntent());
		
		mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapview); // Binding the map fragment
		map = mapFragment.getMap(); // Getting the map from the fragment
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.090087, 34.803000), MIN_ZOOM)); // Set camera center at Shenkar college Ramat-Gan, Israel
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL); // Setting the background map to none so the only thing visible is Shenkar building diagram
		
		LatLngBounds bounds = new LatLngBounds(new LatLng(32.089568, 34.802128), new LatLng(32.090501, 34.803617)); // Setting the overlay position
		map.addGroundOverlay(new GroundOverlayOptions()
			.image(BitmapDescriptorFactory.fromResource(R.drawable.shenkar))
			.positionFromBounds(bounds)
			.transparency((float) 0.1)); // Adding the Shenkar building diagram as overlay on the map in the real position

		map.setOnCameraChangeListener(new OnCameraChangeListener() // Map listener so the user won't lose the school location
		{
			@Override
			public void onCameraChange(CameraPosition camera)
			{
				// Checking if the user scrolled out of the map
				if (map.getProjection().getVisibleRegion().nearLeft.latitude < MIN_LATITUDE || map.getProjection().getVisibleRegion().farLeft.latitude > MAX_LATITUDE)
					map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.090087, 34.803000), MIN_ZOOM)); // Set camera center at Shenkar college Ramat-Gan, Israel
				
				// Checking if the map is too zoomed out
				if (camera.zoom < MIN_ZOOM)
					map.animateCamera(CameraUpdateFactory.zoomTo(MIN_ZOOM));
			}
		});
		
		navigationHandler = new NavigationHandler(this);
		pathHandler = new PathHandler(this, map);
		locationHandler = new LocationHandler(this, map, pathHandler, navigationHandler);
		
		markersHandler = new MarkersHandler(this, map, locationHandler, pathHandler, navigationHandler);
		markersHandler.getMarkersFromCloud();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    // Handle presses on the action bar items
	    switch (item.getItemId())
	    {
	    case R.id.action_stop:
	    	if (navigationHandler.isNavigating())
	    	{
	    		navigationHandler.stopNavigation();
	    		Toast.makeText(getApplicationContext(), "Navigation Stopped!", Toast.LENGTH_SHORT).show();
	    	}
	    	else
	    		Toast.makeText(getApplicationContext(), "Not navigating at the moment.", Toast.LENGTH_SHORT).show();
	    	return true;
	    	
	    case R.id.action_found:
	    	if (!navigationHandler.isNavigating())
	    		locationHandler.findCurrentLocation(null, null);
	    	else
	    		Toast.makeText(getApplicationContext(), "Can't retrieve location while navigating", Toast.LENGTH_SHORT).show();
	    	return true;
	    	
	    case R.id.action_logout:
	    	ParseUser.logOut(); // Logging out the user
			Toast.makeText(getApplicationContext(), "Bye Bye", Toast.LENGTH_SHORT).show();
			map.clear(); // Clearing the map
			startActivity(new Intent(getApplicationContext(), SigninActivity.class)); // Start the sign in activity
			finish(); // Terminate the main activity
	    	return true;
	            
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop()
	{
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
}
