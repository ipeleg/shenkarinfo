package il.ac.shenkar.shenkarinfo;

import il.ac.shenkar.shenkarinfo.dijkstra.DijkstraAlgorithm;
import il.ac.shenkar.shenkarinfo.dijkstra.Edge;
import il.ac.shenkar.shenkarinfo.dijkstra.Graph;
import il.ac.shenkar.shenkarinfo.dijkstra.Vertex;
import il.ac.shenkar.shenkarinfo.markers.MyMarkerArray;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import android.app.ProgressDialog;
import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class PathHandler
{
	private ProgressDialog progressDialog;
	private Context context;
	private MyMarkerArray markersArray;
	
	private GoogleMap map;
	private List<Edge> edges;
	
	public PathHandler(Context context, GoogleMap map)
	{
		this.edges = new ArrayList<Edge>();
		markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
		
		this.map = map;
		this.context = context;
	}
	
	public void buildPath()
	{
		progressDialog = ProgressDialog.show(context, "Setting Markers On Map", "Setting...", false); // Showing progress dialog while building the graph
		downloadMidVertexes(); // Downloading all the mid vertexes of the graphs (Markers which not seen in the map)
	}
	
	public LinkedList<Vertex> getPath(String source, String destination)
	{
		Graph graph = new Graph(markersArray.getAllVertexes(), edges);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(markersArray.getVertexByName(source)); // Build Path from Show Room
		LinkedList<Vertex> path = dijkstra.getPath(markersArray.getVertexByName(destination)); // Get the path from Show Room to Cafeteria
		
		for (Vertex vertex : path)
		{
			System.out.println(vertex);
		}
		
		return path;
	}
	
	/**
	 * Method for downloading all the edges from Parse, getting From & To vertex 
	 */
	private void downloadEdges()
	{
		ParseQuery<ParseObject> getEdgesQuery = ParseQuery.getQuery("Edges");
		getEdgesQuery.findInBackground(new FindCallback<ParseObject>()
		{
			@Override
			public void done(List<ParseObject> edgesFromParse, ParseException arg1)
			{
				for (int i=0 ; i<edgesFromParse.size() ; ++i)
				{
					// Adding the both directions
					addLane("Edge_"+i, edgesFromParse.get(i).getString("From"), edgesFromParse.get(i).getString("To"), 1);
					addLane("Edge_Reverse"+i, edgesFromParse.get(i).getString("To"), edgesFromParse.get(i).getString("From"), 1);
				}
				
				progressDialog.dismiss(); // Close the progress dialog
			}
		});
	}
	
	/**
	 * Method for downloading all the mid vertex from Parse.
	 * Mid vertexes means that they are not seen on the map as markers.
	 */
	private void downloadMidVertexes()
	{
		ParseQuery<ParseObject> getMidMarkersQuery = ParseQuery.getQuery("MidVertexes");
		getMidMarkersQuery.findInBackground(new FindCallback<ParseObject>()
		{
			@Override
			public void done(List<ParseObject> markers, ParseException arg1)
			{
				MyMarkerArray markersArray = MyMarkerArray.getInstance(); // Getting the markers array instance
				
				for (int i=0 ; i<markers.size() ; ++i)
				{
					ParseGeoPoint markerLocation = markers.get(i).getParseGeoPoint("markerPosition"); // Getting the marker coordinates
					double frequency = markers.get(i).getDouble("frequency");
					String markerTitle = markers.get(i).getString("title"); // Getting the mid vertex title
					String markerId = markers.get(i).getObjectId(); // Getting the mid vertex ID
					
					Vertex newMidVertex = new Vertex(markerId, markerTitle, frequency, false); // Creating a vertex for each marker on the map
					
					// Adding a new marker to map according to new vertex details
					Marker newMarker = map.addMarker(new MarkerOptions()
										.position(new LatLng(markerLocation.getLatitude(), markerLocation.getLongitude()))
										.title(markerTitle)
										.icon(BitmapDescriptorFactory.fromResource(R.drawable.star_marker)));
					
					markersArray.addMarker(markerTitle, newMidVertex, newMarker, null);
				}
				
				downloadEdges(); // Downloading all the edges, From & To vertexes
			}
		});
	}
	
	/**
	 * Method for adding an edge (lane) between two vertexes
	 */
	private void addLane(String laneId, String sourceLocNo, String destLocNo, int duration)
	{
		Edge lane = new Edge(laneId, markersArray.getVertexByName(sourceLocNo), markersArray.getVertexByName(destLocNo), duration);
		edges.add(lane);
	}
}
